/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Variant.h"
#include "Log.h"

#include <sstream>

namespace Utils
{
  Variant::~Variant()
  {
    clean();
  }

  Variant::Variant()
    : mType{EType::Null}
  {
  }

  Variant::Variant(int value)
    : mType{EType::Integer}
  {
    mData.valueInt = value;
  }

  Variant::Variant(unsigned int value)
    : mType{EType::UnsignedInteger}
  {
    mData.valueUInt = value;
  }

  Variant::Variant(double value)
    : mType{EType::Double}
  {
    mData.valueDouble = value;
  }

  Variant::Variant(float value)
    : mType{EType::Double}
  {
    mData.valueDouble = value;
  }

  Variant::Variant(bool value)
    : mType{EType::Boolean}
  {
    mData.valueBool = value;
  }

  Variant::Variant(const char* cstr)
    : mType{EType::String}
  {
    mData.valueString = new std::string(cstr);
  }

  Variant::Variant(const std::string& value)
    : mType{EType::String}
  {
    mData.valueString = new std::string(value);
  }

  Variant::Variant(std::string&& value)
    : mType{EType::String}
  {
    mData.valueString = new std::string(std::move(value));
  }

  Variant::Variant(const std::list<Variant>& variantList)
    : mType{EType::VariantList}
  {
    mData.valueVariantList = new std::list<Variant>(variantList.cbegin(), variantList.cend());
  }

  Variant::Variant(const Variant& variant)
  {
    mType = EType::Null;
    *this = variant;
  }

  Variant& Variant::operator=(const Variant& rhs)
  {
    if (this == &rhs)
    {
      return *this;
    }

    clean();

    mType = rhs.mType;

    switch(mType)
    {
      case EType::Null:
      {
        break;
      }
      case EType::Integer:
      {
        mData.valueInt = rhs.mData.valueInt;
        break;
      }
      case EType::UnsignedInteger:
      {
        mData.valueUInt = rhs.mData.valueUInt;
        break;
      }
      case EType::Double:
      {
        mData.valueDouble = rhs.mData.valueDouble;
        break;
      }
      case EType::Boolean:
      {
        mData.valueBool = rhs.mData.valueBool;
        break;
      }
      case EType::String:
      {
        mData.valueString = new std::string(*rhs.mData.valueString);
        break;
      }
      case EType::VariantList:
      {
        mData.valueVariantList = new std::list<Variant>(*rhs.mData.valueVariantList);
        break;
      }
    }

    return *this;
  }

  bool Variant::operator==(const Variant& rhs) const
  {
    if (mType == rhs.type())
    {
      switch (mType)
      {
        case EType::Null:
        {
          return true;
        }
        case EType::Integer:
        {
          return mData.valueInt == rhs.mData.valueInt;
        }
        case EType::UnsignedInteger:
        {
          return mData.valueUInt == rhs.mData.valueUInt;
        }
        case EType::Double:
        {
          return mData.valueDouble == rhs.mData.valueDouble;
        }
        case EType::Boolean:
        {
          return mData.valueBool == rhs.mData.valueBool;
        }
        case EType::String:
        {
          return *mData.valueString == *rhs.mData.valueString;
        }
        case EType::VariantList:
        {
          return *mData.valueVariantList == *rhs.mData.valueVariantList;
        }
      }
    }

    return false;
  }

  bool Variant::operator!=(const Variant& rhs) const
  {
    return !(*this == rhs);
  }

  Variant::EType Variant::type() const
  {
    return mType;
  }

  bool Variant::isNull() const
  {
    return mType == EType::Null;
  }

  bool Variant::isInt() const
  {
    return isSignedInt() || isUnsignedInt();
  }

  bool Variant::isSignedInt() const
  {
    return mType == EType::Integer;
  }

  bool Variant::isUnsignedInt() const
  {
    return mType == EType::UnsignedInteger;
  }

  bool Variant::isDouble() const
  {
    return mType == EType::Double;
  }

  bool Variant::isBool() const
  {
    return mType == EType::Boolean;
  }

  bool Variant::isString() const
  {
    return mType == EType::String;
  }

  bool Variant::isVariantList() const
  {
    return mType == EType::VariantList;
  }

  int Variant::toInt() const
  {
    switch (mType)
    {
      case EType::Integer:
      {
        return mData.valueInt;
      }
      case EType::UnsignedInteger:
      {
        return mData.valueUInt;
      }
      case EType::Double:
      {
        return mData.valueDouble;
      }
      case EType::Boolean:
      {
        return mData.valueBool;
      }
      default:
      {
        return 0;
      }
    }
  }

  unsigned int Variant::toUnsignedInt() const
  {
    switch (mType)
    {
      case EType::Integer:
      {
        return mData.valueInt;
      }
      case EType::UnsignedInteger:
      {
        return mData.valueUInt;
      }
      case EType::Double:
      {
        return mData.valueDouble;
      }
      case EType::Boolean:
      {
        return mData.valueBool;
      }
      default:
      {
        return 0;
      }
    }
  }

  double Variant::toDouble() const
  {
    switch (mType)
    {
      case EType::Integer:
      {
        return mData.valueInt;
      }
      case EType::UnsignedInteger:
      {
        return mData.valueUInt;
      }
      case EType::Double:
      {
        return mData.valueDouble;
      }
      case EType::Boolean:
      {
        return mData.valueBool;
      }
      default:
      {
        return 0.0;
      }
    }
  }

  bool Variant::toBool() const
  {
    switch (mType)
    {
      case EType::Integer:
      {
        return mData.valueInt;
      }
      case EType::UnsignedInteger:
      {
        return mData.valueUInt;
      }
      case EType::Double:
      {
        return mData.valueDouble;
      }
      case EType::Boolean:
      {
        return mData.valueBool;
      }
      default:
      {
        return false;
      }
    }
  }

  std::string Variant::toString() const
  {
    switch (mType)
    {
      case EType::Integer:
      {
        return std::to_string(mData.valueInt);
      }
      case EType::UnsignedInteger:
      {
        return std::to_string(mData.valueUInt);
      }
      case EType::Double:
      {
        return std::to_string(mData.valueDouble);
      }
      case EType::Boolean:
      {
        return mData.valueBool ? "true" : "false";
      }
      case EType::String:
      {
        return *mData.valueString;
      }
      case EType::VariantList:
      {
        std::stringstream stream;
        for (const auto& elem : *mData.valueVariantList)
        {
          if (stream.rdbuf()->in_avail() == 0)
          {
            stream << "[";
          }
          else
          {
            stream << ", ";
          }

          stream << elem.toString();
        }
        stream << "]";
        return stream.str();
      }
      default:
      {
        return std::string();
      }
    }
  }

  std::list<Variant> Variant::toVariantList() const
  {
    std::list<Variant> variantList;

    switch (mType)
    {
      case EType::Integer:
      {
        variantList.push_back(Variant(mData.valueInt));
        break;
      }
      case EType::UnsignedInteger:
      {
        variantList.push_back(Variant(mData.valueUInt));
        break;
      }
      case EType::Double:
      {
        variantList.push_back(Variant(mData.valueDouble));
        break;
      }
      case EType::Boolean:
      {
        variantList.push_back(Variant(mData.valueBool));
        break;
      }
      case EType::String:
      {
        variantList.push_back(Variant(*mData.valueString));
        break;
      }
      case EType::VariantList:
      {
        return *mData.valueVariantList;
      }
      default:
      {
        break;
      }
    }

    return variantList;
  }

  void Variant::clean()
  {
    switch (mType)
    {
      case EType::String:
      {
        delete mData.valueString;
        mData.valueString = nullptr;
        break;
      }
      case EType::VariantList:
      {
        delete mData.valueVariantList;
        mData.valueVariantList = nullptr;
        break;
      }
      default:
      {
        break;
      }
    }
  }
}
