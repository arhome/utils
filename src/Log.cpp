/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Log.h"

#include <fstream>
#include <iostream>
#include <chrono>
#include <memory>
#include <stdexcept>

namespace Utils
{
  namespace Log
  {
    namespace
    {
      class StdoutStream : public StreamBuffer
      {
      public:
        ~StdoutStream() override = default;
        void doFlush() override;
      };

      class FileStream : public StreamBuffer
      {
      public:
        explicit FileStream(std::string aLogFileName);
        ~FileStream() override;
        void doFlush() override;

      private:
        std::ofstream mFileStream;
      };

      class NullStream : public StreamBuffer
      {
      public:
        ~NullStream() override = default;
        void doFlush() override {}
      };


      /*!
       * @brief Create new stream
       * @details Usage makeStream<StreamType>(StreamArgs)
       * @tparam TStream the stream to instantiate
       * @tparam TArgs the stream arguments
       */
      template<typename TStream, typename ...TArgs>
      std::shared_ptr<StreamBuffer> makeStream(TArgs&&... aArgs)
      {
        return std::make_shared<TStream>(std::forward<TArgs>(aArgs)...);
      }

      std::shared_ptr<StreamBuffer> streamBuffer {makeStream<NullStream>()};
    }

    void setStdoutLog()
    {
      streamBuffer = makeStream<StdoutStream>();
    }

    void setFileLog(const std::string& aFilePath)
    {
      streamBuffer = makeStream<FileStream>(aFilePath);
    }

    void setNullLog()
    {
      streamBuffer = makeStream<NullStream>();
    }

    void disableLog()
    {
      setNullLog();
    }

    LogStream GetLogStream()
    {
      return LogStream(streamBuffer);
    }

    namespace
    {
      std::string logLevelToString(const Level aLevel)
      {
        switch(aLevel)
        {
          case Level::Error:   return "<ERROR>   ";
          case Level::Warning: return "<WARNING> ";
          case Level::Debug:   return "<DEBUG>   ";
        }

        return {};
      }

      std::string createLogTime()
      {
        auto now = std::chrono::system_clock::now();
        auto nowTime = std::chrono::system_clock::to_time_t(now);
        std::string logTime = std::ctime(&nowTime);

        auto msSinceEpoch = std::chrono::system_clock::now().time_since_epoch() /
          std::chrono::milliseconds(1);

        return logTime.substr(0 , logTime.size() - 1) + " " + std::to_string(msSinceEpoch); // remove end line
      }

      std::string createLogHeader(const Level aLevel)
      {
        return createLogTime() + " " + logLevelToString(aLevel) + ": ";
      }
    }

    void StdoutStream::doFlush()
    {
      switch(mLevel)
      {
        case Level::Error:
          std::cerr << createLogHeader(mLevel) << str() << std::endl;
          break;
        default:
          std::cout << createLogHeader(mLevel) << str() << std::endl;
          break;
      }
    }

    FileStream::FileStream(std::string aLogFileName)
    {
      mFileStream.open(aLogFileName.c_str(), std::ios_base::binary|std::ios_base::out);

      if (!mFileStream.is_open())
      {
        throw (std::runtime_error("Unable to open an output stream"));
      }
    }

    FileStream::~FileStream()
    {
      mFileStream.close();
    }

    void FileStream::doFlush()
    {
      mFileStream << createLogHeader(mLevel);
      mFileStream << str() << std::endl;
      mFileStream.flush();
    }

    StreamBuffer& StreamBuffer::operator<<(Level aLogLevel)
    {
      mLevel = aLogLevel;
      return *this;
    }

    void StreamBuffer::flush()
    {
      if (str().size())
      {
        doFlush();
        str({});
      }
    }

    LogStream::LogStream(std::shared_ptr<StreamBuffer> aStreamBuffer)
      : mStreamBuffer(aStreamBuffer) {}

    LogStream::~LogStream()
    {
      mStreamBuffer->flush();
    }

    StreamBuffer& LogStream::operator<<(Level aLevel)
    {
      *mStreamBuffer << aLevel;
      return *mStreamBuffer;
    }
  }
}
