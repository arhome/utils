/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "CurlHttpClient.h"

#include "HttpConstants.h"
#include "Log.h"
#include "Settings.h"
#include "Utils.h"
#include "Variant.h"

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>
#include <curlpp/Infos.hpp>

#include <array>
#include <iostream>
#include <list>
#include <memory>
#include <ostream>
#include <sstream>
#include <string>

namespace Utils
{
  namespace Network
  {
    namespace
    {
      const bool KCurlHttpVerbose = false;
      const bool KCurlHttpFollowLocation = true;
      const unsigned int kDefaultTimeoutMs = 5000;
    }

    SharedHttpClient CreateHttpClient(const Settings& settings)
    {
      return std::make_unique<CurlHttpClient>(settings);
    }

    CurlHttpClient::CurlHttpClient()
      : mTimeoutMs{kDefaultTimeoutMs}
    {
    }

    CurlHttpClient::CurlHttpClient(const Settings& settings)
      : mTimeoutMs{kDefaultTimeoutMs}
    {
      auto timeoutSetting = settings.get(SETTING_NETWORK_HTTPCLIENT_TIMEOUT);
      if (timeoutSetting.isInt())
      {
        mTimeoutMs = timeoutSetting.toInt();
      }
    }

    bool CurlHttpClient::isAvailable(const Url& url) const
    {
      bool available = false;

      const auto httpData = get(url, Data(), Headers());
      if (httpData.code == Http::StatusCode::SuccessOk)
      {
        available = true;
      }

      return available;
    }

    HttpResponse CurlHttpClient::get(const Url& url,
                                     const Data& dataIn,
                                     const Headers& headerIn) const
    {
      LOG_DEBUG() << "HttpClient::get " << url;
      HttpResponse httpResponseOut;
      doGet(httpResponseOut, url, dataIn, headerIn);
      return httpResponseOut;
    }

    void CurlHttpClient::doGet(HttpResponse& httpResponseOut, const Url& url,
                               const Data& dataIn, const Headers& headerIn) const
    {
      try
      {
        curlpp::Cleanup cleaner; Utils::ignore(cleaner);
        curlpp::Easy curlppRequest;

        curlppRequest.setOpt(new curlpp::options::Url(url));
        curlppRequest.setOpt(new curlpp::options::Header(true));
        curlppRequest.setOpt(new curlpp::options::Verbose(KCurlHttpVerbose));
        curlppRequest.setOpt(new curlpp::options::FollowLocation(KCurlHttpFollowLocation));

        curlppRequest.setOpt(new curlpp::options::Timeout(mTimeoutMs));

        // Add headers
        curlppRequest.setOpt(new curlpp::options::HttpHeader(headerIn));

        // Add post data
        if (!dataIn.empty())
        {
          curlppRequest.setOpt(new curlpp::options::PostFields(dataIn));
          curlppRequest.setOpt(new curlpp::options::PostFieldSize(dataIn.size()));
        }

        // Add writer
        std::stringstream outputStream;
        const curlpp::options::WriteStream writerStream(&outputStream);
        curlppRequest.setOpt(writerStream);

        curlppRequest.perform();

        // Get header and data
        const auto headerSize {curlpp::infos::HeaderSize::get(curlppRequest)};
        const auto dataSize {(outputStream.str().size() - headerSize)};

        httpResponseOut.code = curlpp::infos::ResponseCode::get(curlppRequest);

        if (headerSize > 0)
        {
          httpResponseOut.header = outputStream.str();
        }

        if (dataSize > 0)
        {
          httpResponseOut.data = outputStream.str().substr(headerSize);
        }
      }
      catch (curlpp::LogicError& curlppLogicExcept)
      {
        LOG_DEBUG() << curlppLogicExcept.what();
      }
      catch (curlpp::RuntimeError& curlppRuntimeExcept)
      {
        LOG_DEBUG() << curlppRuntimeExcept.what();
      }
    }
  }
}
