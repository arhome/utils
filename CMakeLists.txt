# Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

cmake_minimum_required(VERSION 3.8)
project(utils)

# Enable tests -DUTILS_TEST=ON
option(UTILS_TEST "Build all tests" OFF)
# Enable doc -DUTILS_DOC=ON
option(UTILS_DOC  "Generate documentation" OFF)

include(common-config.cmake)
subdirlist(EXT_MODULE_PATHS ${CMAKE_SOURCE_DIR}/ext)
set(CMAKE_MODULE_PATH ${EXT_MODULE_PATHS} ${CMAKE_SOURCE_DIR}/ext)

# Add external dependencies
include(Curlpp)
include(Googletest)
include(NlohmannJson)

list(APPEND LIBUTILS_INCLUDES
  ${CMAKE_SOURCE_DIR}/include
  )

list(APPEND LIBUTILS_HEADERS
  include/HttpClient.h
  include/HttpConstants.h
  include/Log.h
  include/Settings.h
  include/SettingsImpl.h
  include/SettingsJson.h
  include/Utils.h
  include/Variant.h
  include/Worker.h
  )

list(APPEND LIBUTILS_SOURCES
  src/CurlHttpClient.cpp
  src/CurlHttpClient.h
  src/Log.cpp
  src/Variant.cpp
  )

add_library(${PROJECT_NAME}
  ${LIBUTILS_HEADERS}
  ${LIBUTILS_SOURCES}
  )

target_link_libraries(${PROJECT_NAME}
  PUBLIC
  ${CURL_LIBRARIES}
  curlpp_static
  )

target_include_directories(${PROJECT_NAME}
  PUBLIC
  "$<BUILD_INTERFACE:${LIBUTILS_INCLUDES}>"
  )

set(CMAKE_INSTALL_PREFIX ${PROJECT_BINARY_DIR}/${PROJECT_NAME})
install(TARGETS ${PROJECT_NAME}
  DESTINATION lib
  )

install(FILES ${LIBUTILS_HEADERS}
  DESTINATION include/${PROJECT_NAME}
  )

if (UTILS_TEST)
  set(CMAKE_BUILD_TYPE Debug)
  include(CodeCoverage)

  append_coverage_compiler_flags()

  set(COVERAGE_EXCLUDES
    '${CMAKE_BINARY_DIR}/nlohmann_json-src/*'
    '${CMAKE_BINARY_DIR}/googletest-src/*'
    '${CMAKE_BINARY_DIR}/curlpp-src/*'
    '${CMAKE_CURRENT_SOURCE_DIR}/test/*'
    '/usr/*'
    )

  set(UTILS_COVERAGE
    ${PROJECT_NAME}_coverage
    )

  setup_target_for_coverage(NAME ${UTILS_COVERAGE}
    EXECUTABLE utils_test
    )

  add_subdirectory(test)
endif()

if (UTILS_DOC)
  find_package(Doxygen)

  set(DOXYGEN_GENERATE_HTML YES)

  doxygen_add_docs(
    doxygen
    src/ include/ README.md
    COMMENT "Generate html pages"
)
endif()
