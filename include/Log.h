/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef LOG_H
#define LOG_H

#include <memory>
#include <sstream>
#include <string>

namespace Utils
{
  namespace Log
  {
    enum class Level
    {
      Debug,
      Warning,
      Error
    };

    class StreamBuffer : public std::ostringstream
    {
    public:
      ~StreamBuffer() override = default;

      StreamBuffer& operator<<(Level aLogLevel);

      void flush();

      virtual void doFlush() = 0;

    protected:
      Level mLevel {Level::Debug};
    };

    class LogStream
    {
    public:
      explicit LogStream(std::shared_ptr<StreamBuffer> aStreamBuffer);
      ~LogStream();

      StreamBuffer& operator<<(Level aLevel);

    private:
      std::shared_ptr<StreamBuffer> mStreamBuffer;
    };

//! Select output file to store logging messages
//! @details A file is created. Logs are stored with a specific header including the date,
//! a timestamp and the severity level.
    void setFileLog(const std::string& filePath);

//! Select the standard output to display logging messages
//! @details Logs are displayed with a specific header including the date, a timestamp
//! and the severity level.
    void setStdoutLog();

//! @brief Disable logging messages
//! @details The NullLoggerPolicy is set, this is the default state
//! if no logging output has been set.
    void disableLog();

    LogStream GetLogStream();
  }
}

#define LOG_ERROR()                                   \
  Utils::Log::GetLogStream()                          \
  << Utils::Log::Level::Error

#define LOG_WARNING()                                 \
  Utils::Log::GetLogStream()                          \
  << Utils::Log::Level::Warning

#define LOG_DEBUG()                                   \
  Utils::Log::GetLogStream()                          \
  << Utils::Log::Level::Debug

#endif // LOG_H
