/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef WORKER_H
#define WORKER_H

#include "Log.h"

#include <chrono>
#include <functional>
#include <future>
#include <memory>
#include <thread>

namespace Utils
{
  template <typename T>
  class WorkerObserver
  {
  public:
    virtual void OnWorkerFinished(T&& returnedValue) const = 0;
  };

  class Worker
  {
  public:
    virtual ~Worker() = default;
    Worker(): mThread{} {}
    Worker(const Worker&) = delete;
    Worker& operator=(const Worker&) = delete;
    Worker(Worker&&) = delete;
    Worker& operator=(Worker&&) = delete;

    template <typename Obs, typename Functor, typename... Args>
    void run(const Obs& obs, const Functor& func, Args&&... args)
    {
      LOG_DEBUG() << __PRETTY_FUNCTION__;
      if (!mThread)
      {
        std::packaged_task<void()> task(
          std::bind([&obs, &func](auto&&...args){
              LOG_DEBUG() << "call func";
              auto ret = func(decltype(args)(args)...);
              LOG_DEBUG() << "call OnWorkerFinished";
              obs.OnWorkerFinished(std::move(ret));
            }, std::forward<Args>(args)...));
        mThread = std::make_unique<std::thread>(std::move(task));
      }
    }

    template <typename Obs, typename Functor, typename... Args>
    void run_async(const Obs& obs, const Functor& func, Args&&... args)
    {
      LOG_DEBUG() << __PRETTY_FUNCTION__;
      if (!mThread)
      {
        run(obs, func, std::forward<Args>(args)...);
        mThread->detach();
      }
    }

    void exit()
    {
      if (mThread && mThread->joinable())
      {
        mThread->join();
      }
    }

  private:
    std::unique_ptr<std::thread> mThread;
  };

  class WorkerPeriodic
  {
  public:
    virtual ~WorkerPeriodic() = default;
    WorkerPeriodic(): mThread{} {}
    WorkerPeriodic(const WorkerPeriodic&) = delete;
    WorkerPeriodic& operator=(const WorkerPeriodic&) = delete;
    WorkerPeriodic(WorkerPeriodic&&) = delete;
    WorkerPeriodic& operator=(WorkerPeriodic&&) = delete;

    template <typename T, class Functor, class... Args>
    void run(const WorkerObserver<T>& obs, const unsigned int intervalMs, const unsigned int nb, Functor&& func, Args&&... args)
    {
      if (!mThread)
      {
        using zF = std::decay_t<Functor>&&;
        using R = std::result_of_t< zF(Args...) >;
        std::packaged_task<R()> task(
          std::bind([&obs, intervalMs, nb, func = std::forward<Functor>(func)](auto&&...args)mutable->R{
              for (unsigned int loop = 0; loop < nb; ++loop)
              {
                auto start = std::chrono::system_clock::now();
                auto ret = std::move(func)(decltype(args)(args)...);
                obs.OnWorkerFinished(std::move(ret));
                auto end = std::chrono::system_clock::now();
                auto elapsedMs = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
                std::chrono::milliseconds sleepMs(intervalMs - elapsedMs);
                LOG_DEBUG() << "Periodic worker sleep for:" << sleepMs.count();
                std::this_thread::sleep_for(sleepMs);
              }
            }, std::forward<Args>(args)...));
        mThread = std::make_unique<std::thread>(std::move(task));
      }
    }

    template <typename T, class Functor, class... Args>
    void run_async(const WorkerObserver<T>& obs, const unsigned int intervalMs, const unsigned int nb, Functor&& func, Args&&... args)
    {
      if (!mThread)
      {
        run(obs, intervalMs, nb, std::forward<Functor>(func), std::forward<Args>(args)...);
        mThread->detach();
      }
    }

    void exit()
    {
      if (mThread && mThread->joinable())
      {
        mThread->join();
      }
    }

  private:
    std::unique_ptr<std::thread> mThread;
  };
}

#endif // WORKER_H
