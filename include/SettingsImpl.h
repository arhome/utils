/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SETTINGSIMPL_H
#define SETTINGSIMPL_H

#include "Log.h"

#include <iterator>
#include <map>
#include <utility>

/*!
  @brief namespace for Utils
  @see https://gitlab.com/arhome/utils
*/
namespace Utils
{
  class SettingsParser{};
  class SettingsSerializer{};

/*!
  @brief A class to wrap settings with settings parser (@ref TParser) and settings
  serializer (@ref TSerializer)

  @tparam TSettingsBase type for setting object (@ref BasicSetting by default)
  @tparam TParser the parser to resolve calls to `load()` (@ref SettingsParser
  by default)
  @tparam TSerializer the serializer to resolve calls to `save()`
  (@ref SettingsSerializer by default)
*/
  template<typename TSettingsBase,
           typename TParser,
           typename TSerializer>
  class SettingsWrapper : public TSettingsBase
  {
  public:
    SettingsWrapper() noexcept = default;
    explicit SettingsWrapper(const TSettingsBase& settingsBase) noexcept
      : TSettingsBase(settingsBase) {}
    explicit SettingsWrapper(TSettingsBase&& settingsBase) noexcept
      : TSettingsBase(std::forward<TSettingsBase>(settingsBase)) {}

    SettingsWrapper& operator=(const SettingsWrapper&) = default;
    SettingsWrapper& operator=(SettingsWrapper&&) = default;

    template<typename ...TArgs>
    void load(const TArgs&... args)
    {
      TParser(*this, args...);
    }

    template<typename ...TArgs>
    void save(const TArgs&... args)
    {
      TSerializer(*this, args...);
    }
  };

  template<typename TKey, typename TValue>
  class BasicSetting
  {
  public:
    using key_t = TKey;
    using value_t = TValue;
    using container_t = std::map<key_t, value_t>;
    using iterator = typename container_t::iterator;
    using const_iterator = typename container_t::const_iterator;
    using reverse_iterator = typename container_t::reverse_iterator;
    using const_reverse_iterator = typename container_t::const_reverse_iterator;

  public:
    BasicSetting() noexcept = default;
    BasicSetting(const BasicSetting&) = default;
    BasicSetting(BasicSetting&&) noexcept = default;
    explicit BasicSetting(std::initializer_list<typename container_t::value_type> init)
    {
      mSettingsMap.insert(init);
    }

    BasicSetting& operator=(const BasicSetting&) = default;
    BasicSetting& operator=(BasicSetting&&) noexcept = default;

    bool has(const key_t& id) const
    {
      if (mSettingsMap.find(id) != mSettingsMap.end())
      {
        return true;
      }
      return false;
    }

    value_t get(const key_t& id) const
    {
      value_t setting;
      auto iter = mSettingsMap.find(id);
      if (iter != mSettingsMap.end())
      {
        setting = iter->second;
      }
      return setting;
    }

    void set(const key_t& id, const value_t& value)
    {
      if (!has(id))
      {
        LOG_WARNING() << "Insert new id:" + id + "(should be added in default settings).";
      }

      mSettingsMap[id] = value;
    }

    iterator begin() noexcept {return mSettingsMap.begin();}
    iterator end() noexcept {return mSettingsMap.end();}

    const_iterator begin() const noexcept {return mSettingsMap.begin();}
    const_iterator end() const noexcept {return mSettingsMap.end();}

    const_iterator cbegin() const noexcept {return mSettingsMap.cbegin();}
    const_iterator cend() const noexcept {return mSettingsMap.cend();}

    reverse_iterator rbegin() noexcept {return mSettingsMap.rbegin();}
    reverse_iterator rend() noexcept {return mSettingsMap.rend();}

    const_reverse_iterator rbegin() const noexcept {return mSettingsMap.rbegin();}
    const_reverse_iterator rend() const noexcept {return mSettingsMap.rend();}

    const_reverse_iterator crbegin() const noexcept {return mSettingsMap.crbegin();}
    const_reverse_iterator crend() const noexcept {return mSettingsMap.crend();}

  private:
    container_t mSettingsMap;
  };
}

#endif // SETTINGSIMPL_H
