#pragma once
/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef UTILS_H
#define UTILS_H

#include <algorithm>
#include <list>
#include <map>
#include <string>
#include <sstream>

namespace Utils
{
  template <typename... Args>
  void ignore(Args&&...) {}

  template <typename T>
  std::string join(const T& container, const std::string& sep)
  {
    std::stringstream ss;
    for (const auto& elem : container)
    {
      if (ss.rdbuf()->in_avail() != 0)
      {
        ss << sep;
      }
      ss << elem;
    }
    return ss.str();
  }

  template<typename T>
  inline std::list<T> operator-(const std::list<T>& lhs, const std::list<T>& rhs)
  {
    std::list<T> diff;
    std::set_difference(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::back_inserter(diff));
    return diff;
  }
}

#endif // UTILS_H
