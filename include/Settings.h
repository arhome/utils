/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include "SettingsImpl.h"
#include "SettingsJson.h"
#include "Variant.h"

#include <string>

/*!
 * @brief namespace for Utils
 * @see https://gitlab.com/arhome/utils
 */
namespace Utils
{
  using SettingKey = std::string;
  using SettingValue = Utils::Variant;

  using SettingsImpl = Utils::BasicSetting<SettingKey, SettingValue>;
  using SettingsJsonParser = Utils::JsonParser<SettingsImpl>;
  using SettingsJsonSerializer = Utils::JsonSerializer<SettingsImpl>;

  using Settings = Utils::SettingsWrapper<SettingsImpl,
                                          SettingsJsonParser,
                                          SettingsJsonSerializer>;

  const SettingKey SETTING_NETWORK_HTTPCLIENT_TIMEOUT  {"network.timeout"};
}

#endif // SETTINGS_H
