/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef VARIANT_H
#define VARIANT_H

#include <list>
#include <memory>
#include <string>

namespace Utils
{
  class Variant
  {
  public:
    enum class EType
    {
      Null,
      Integer,
      UnsignedInteger,
      Double,
      Boolean,
      String,
      VariantList
    };

  public:
    ~Variant();
    Variant();
    Variant(int value);
    Variant(unsigned int value);
    Variant(double value);
    Variant(float value);
    Variant(bool value);
    Variant(const char* cstr);
    Variant(const std::string& value);
    Variant(std::string&& value);
    Variant(const std::list<Variant>& variantList);
    Variant(const Variant& variant);

    Variant& operator=(const Variant& rhs);
    bool operator==(const Variant& rhs) const;
    bool operator!=(const Variant& rhs) const;

    EType type() const;

    bool isNull() const;
    bool isInt() const;
    bool isSignedInt() const;
    bool isUnsignedInt() const;
    bool isDouble() const;
    bool isBool() const;
    bool isString() const;
    bool isVariantList() const;

    int toInt() const;
    unsigned int toUnsignedInt() const;
    double toDouble() const;
    bool toBool() const;
    std::string toString() const;
    std::list<Variant> toVariantList() const;

  private:
    void clean();

    union UVariant
    {
      int valueInt;
      unsigned int valueUInt;
      double valueDouble;
      bool valueBool;
      std::string* valueString;
      std::list<Variant>* valueVariantList;
    } mData;

    EType mType;
  };
}

#endif // VARIANT_H
