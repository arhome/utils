/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SETTINGSJSON_H
#define SETTINGSJSON_H

#include "Log.h"
#include "Settings.h"
#include "Utils.h"
#include "Variant.h"

#include <nlohmann/json.hpp>

#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <string>

namespace Utils
{
  const unsigned int KJsonIndentLevelInSpaces = 2;

  inline nlohmann::json toJson(const Variant& variant)
  {
    switch(variant.type())
    {
      case Variant::EType::Integer:
      {
        return variant.toInt();
      }
      case Variant::EType::Double:
      {
        return variant.toDouble();
      }
      case Variant::EType::Boolean:
      {
        return variant.toBool();
      }
      case Variant::EType::String:
      {
        return variant.toString();
      }
      case Variant::EType::VariantList:
      {
        nlohmann::json subjson;
        for (const auto& variantElt : variant.toVariantList())
        {
          subjson.push_back(toJson(variantElt));
        }
        return subjson;
      }
      default:
      {
        return nlohmann::json();
      }
    }
  }

  inline Variant fromJson(const nlohmann::json& jsonObj)
  {
    switch(jsonObj.type())
    {
      case nlohmann::json::value_t::number_integer:
      {
        return Variant(jsonObj.get<int>());
      }
      case nlohmann::json::value_t::number_unsigned:
      {
        return Variant(jsonObj.get<unsigned int>());
      }
      case nlohmann::json::value_t::number_float:
      {
        return Variant(jsonObj.get<double>());
      }
      case nlohmann::json::value_t::boolean:
      {
        return Variant(jsonObj.get<bool>());
      }
      case nlohmann::json::value_t::string:
      {
        return Variant(jsonObj.get<std::string>());
      }
      case nlohmann::json::value_t::array:
      {
        std::list<Variant> variantList;
        for (const auto& subJsonObj : jsonObj)
        {
          variantList.push_back(fromJson(subJsonObj));
        }
        return Variant(variantList);
      }
      default:
      {
        return Variant();
      }
    }
  }

  inline std::string getMajor(const std::string& str)
  {
    auto pos = str.find('.');
    if (pos != std::string::npos)
    {
      // "major.minor.mminor" -> "major"
      return str.substr(0, pos);
    }

    // "major" -> "major"
    return str;
  }

  inline std::string getMinor(const std::string& str)
  {
    auto pos = str.find('.');
    if (pos != std::string::npos)
    {
      // "major.minor.mminor" -> "minor.mminor"
      return str.substr(pos + 1);
    }

    // "major" -> ""
    return std::string();
  }

  template<typename TSettings>
  nlohmann::json serialize(const TSettings& settings)
  {
    nlohmann::json obj;
    std::map<std::string, TSettings> submap;

    for (const auto& subsetting : settings)
    {
      auto keyMajor = getMajor(subsetting.first); //[major].minor.minor
      auto keyMinor = getMinor(subsetting.first); //major.[minor.minor]
      if (obj.count(keyMajor) == 0)
      {
        if (keyMinor.empty())
        {
          obj[keyMajor] = toJson(subsetting.second);
        }
        else
        {
          submap[keyMajor].set(keyMinor, subsetting.second);
        }
      }
    }

    for (const auto& setting : submap)
    {
      obj[setting.first] = serialize(setting.second);
    }

    return obj;
  }

  template<typename TSettings>
  TSettings deserialize(const nlohmann::json& obj)
  {
    TSettings map;
    std::map<std::string, nlohmann::json> submap;

    for (nlohmann::json::const_iterator it = obj.cbegin(); it != obj.cend(); ++it)
    {
      if (!it.value().is_object()) // Subobject
      {
        map.set(it.key(), fromJson(it.value()));
      }
      else
      {
        submap[it.key()] = it.value();
      }
    }

    for (const auto& submapElt : submap)
    {
      auto tmpmap = deserialize<TSettings>(submapElt.second);
      for (const auto& tmpkey : tmpmap)
      {
        auto mapkey = submapElt.first + '.' + tmpkey.first;
        map.set(mapkey, tmpmap.get(tmpkey.first));
      }
    }

    return map;
  }

  template<typename TSettings>
  class JsonParser
  {
  public:
    template<typename TFirst, typename ...>
    JsonParser(TSettings& settings, const TFirst& filePath)
    {
      LOG_DEBUG() << "Setting JsonParser from " << filePath;

      std::ifstream inputFile(filePath, std::ios::in);
      if (!inputFile.is_open())
      {
        LOG_WARNING() << "Couldn't open settings file:" + filePath;
        return;
      }

      nlohmann::json jsonObj;
      inputFile >> jsonObj;

      settings = deserialize<TSettings>(jsonObj);
    }
  };

  template<typename TSettings>
  class JsonSerializer
  {
  public:
    template<typename TFirst, typename ...>
    JsonSerializer(const TSettings& settings, const TFirst& filePath)
    {
      LOG_DEBUG() << "Setting JsonSerializer to " << filePath;

      std::ofstream outputFile(filePath, std::ios::out);
      outputFile << serialize<TSettings>(settings).dump(KJsonIndentLevelInSpaces);
    }
  };
}

#endif // SETTINGSJSON_H
