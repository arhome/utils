/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "WorkerTest.h"

#include "Log.h"
#include "Worker.h"

#include <memory>
#include <string>
#include <thread>

using namespace UtilsTest;

TEST_F(WorkerTest, SingleWorkerTest)
{
  TestWorkerObserver<int> obs;
  auto newWorker {std::make_unique<Utils::Worker>()};
  newWorker->run(obs,
                 [](int a, int b, std::string str){
                   LOG_DEBUG() << "callback: " << a << str << b; return a+b;} , 1, 2, "+");
  LOG_DEBUG() << "WorkerTest";
  newWorker->exit();
}

TEST_F(WorkerTest, SingleAsyncWorkerTest)
{
  TestWorkerObserver<int> obs;
  auto newWorker {std::make_unique<Utils::Worker>()};
  newWorker->run_async(obs, [](int a, int b){return a+b;}, 10, 20);
  LOG_DEBUG() << "WorkerAsyncTest";
  std::this_thread:: sleep_for(std::chrono::seconds(1));
}

TEST_F(WorkerTest, WorkerPeriodicTest)
{
  TestWorkerObserver<int> obs;
  auto newWorker {std::make_unique<Utils::WorkerPeriodic>()};

  auto start = std::chrono::system_clock::now();
  auto periode = 100;
  auto count = 5;
  newWorker->run(obs, periode, count,
                 [](){
                   std::this_thread:: sleep_for(std::chrono::milliseconds(50));
                   LOG_DEBUG() << "Periodic callback";
                   return 0;});
  LOG_DEBUG() << "WorkerPeriodicTest";
  newWorker->exit();
  auto end = std::chrono::system_clock::now();
  auto elapsedMs = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

  EXPECT_LE(elapsedMs, periode * count + count);
}
