/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "HttpClientTest.h"

#include "HttpClient.h"
#include "Log.h"

using namespace UtilsTest;

namespace
{
  const std::string KSettingsFilePath {"./settings_test.json"};
}

CCurlHttpClientTest::CCurlHttpClientTest()
  : mHttpClient{Utils::Network::CreateHttpClient(mSettings)}
{
}

TEST_F(CCurlHttpClientTest, TestGetUrl)
{
  mHttpClient->get("https://www.google.com");
}

TEST_F(CCurlHttpClientTest, TestIsAvailable)
{
  EXPECT_TRUE(mHttpClient->isAvailable("https://www.google.com"));
  EXPECT_TRUE(mHttpClient->isAvailable("https://www.wikipedia.org"));
  EXPECT_FALSE(mHttpClient->isAvailable("https://www.invalidaddress.moc"));
}

