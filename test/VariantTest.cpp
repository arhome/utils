/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "VariantTest.h"

#include "Variant.h"

#include <limits>
#include <list>
#include <string>

using namespace Utils;
using namespace UtilsTest;

namespace
{
  void testVariantInt(int value)
  {
    Variant variant(value);
    EXPECT_TRUE(variant.isInt());
    EXPECT_TRUE(variant.isSignedInt());
    EXPECT_EQ(variant.type(), Variant::EType::Integer);
    EXPECT_EQ(variant.toInt(), value);
    EXPECT_EQ(variant.toUnsignedInt(), value);
    EXPECT_EQ(variant.toDouble(), (double)value);
    EXPECT_EQ(variant.toBool(), (bool)value);
    EXPECT_EQ(variant.toString(), std::to_string(value));
    EXPECT_EQ(variant.toVariantList().front().toInt(), value);

    auto copyCtor(variant);
    EXPECT_EQ(copyCtor, variant);
    EXPECT_TRUE(copyCtor.isInt());
    EXPECT_EQ(copyCtor.toInt(), value);

    auto copyAffect = variant;
    EXPECT_EQ(copyAffect, variant);
    EXPECT_TRUE(copyAffect.isSignedInt());
    EXPECT_EQ(copyAffect.toInt(), value);
  }

  void testVariantUnsignedInt(unsigned int value)
  {
    Variant variant(value);
    EXPECT_TRUE(variant.isInt());
    EXPECT_TRUE(variant.isUnsignedInt());
    EXPECT_EQ(variant.type(), Variant::EType::UnsignedInteger);
    EXPECT_EQ(variant.toInt(), value);
    EXPECT_EQ(variant.toUnsignedInt(), value);
    EXPECT_EQ(variant.toDouble(), (double)value);
    EXPECT_EQ(variant.toBool(), (bool)value);
    EXPECT_EQ(variant.toString(), std::to_string(value));
    EXPECT_EQ(variant.toVariantList().front().toUnsignedInt(), value);

    auto copyCtor(variant);
    EXPECT_EQ(copyCtor, variant);
    EXPECT_TRUE(copyCtor.isUnsignedInt());
    EXPECT_EQ(copyCtor.toUnsignedInt(), value);

    auto copyAffect = variant;
    EXPECT_EQ(copyAffect, variant);
    EXPECT_TRUE(copyAffect.isUnsignedInt());
    EXPECT_EQ(copyAffect.toUnsignedInt(), value);
  }

  template<typename T>
  void testVariantReal(T value)
  {
    Variant variant(value);
    EXPECT_TRUE(variant.isDouble());
    EXPECT_EQ(variant.type(), Variant::EType::Double);
    EXPECT_EQ(variant.toInt(), (int)value);
    EXPECT_EQ(variant.toUnsignedInt(), (unsigned int)value);
    EXPECT_EQ(variant.toDouble(), value);
    EXPECT_EQ(variant.toBool(), (bool)value);
    EXPECT_EQ(variant.toString(), std::to_string(value));
    EXPECT_EQ(variant.toVariantList().front().toDouble(), value);

    auto copyCtor(variant);
    EXPECT_EQ(copyCtor, variant);
    EXPECT_TRUE(copyCtor.isDouble());
    EXPECT_EQ(copyCtor.toDouble(), value);

    auto copyAffect = variant;
    EXPECT_EQ(copyAffect, variant);
    EXPECT_TRUE(copyAffect.isDouble());
    EXPECT_EQ(copyAffect.toDouble(), value);
  }

  void testVariantBool(bool value)
  {
    Variant variant(value);
    EXPECT_TRUE(variant.isBool());
    EXPECT_EQ(variant.type(), Variant::EType::Boolean);
    EXPECT_EQ(variant.toInt(), (int)value);
    EXPECT_EQ(variant.toUnsignedInt(), (unsigned int)value);
    EXPECT_EQ(variant.toDouble(), (double)value);
    EXPECT_EQ(variant.toBool(), value);
    EXPECT_EQ(variant.toString(), value ? "true" : "false");
    EXPECT_EQ(variant.toVariantList().front().toBool(), value);

    auto copyCtor(variant);
    EXPECT_EQ(copyCtor, variant);
    EXPECT_TRUE(copyCtor.isBool());
    EXPECT_EQ(copyCtor.toBool(), value);

    auto copyAffect = variant;
    EXPECT_EQ(copyAffect, variant);
    EXPECT_TRUE(copyAffect.isBool());
    EXPECT_EQ(copyAffect.toBool(), value);
  }

  template<typename T>
  void testVariantString(T value)
  {
    Variant variant(value);
    EXPECT_TRUE(variant.isString());
    EXPECT_EQ(variant.type(), Variant::EType::String);
    EXPECT_EQ(variant.toInt(), 0);
    EXPECT_EQ(variant.toUnsignedInt(), 0);
    EXPECT_EQ(variant.toDouble(), 0.0);
    EXPECT_EQ(variant.toBool(), false);
    EXPECT_EQ(variant.toString(), std::string(value));
    EXPECT_EQ(variant.toVariantList().front().toString(), std::string(value));

    auto copyCtor(variant);
    EXPECT_EQ(copyCtor, variant);
    EXPECT_TRUE(copyCtor.isString());
    EXPECT_EQ(copyCtor.toString(), value);

    auto copyAffect = variant;
    EXPECT_EQ(copyAffect, variant);
    EXPECT_TRUE(copyAffect.isString());
    EXPECT_EQ(copyAffect.toString(), value);
  }

  void testVariantList(const std::list<Variant>& value)
  {
    Variant variant(value);
    EXPECT_TRUE(variant.isVariantList());
    EXPECT_EQ(variant.type(), Variant::EType::VariantList);
    EXPECT_EQ(variant.toInt(), 0);
    EXPECT_EQ(variant.toUnsignedInt(), 0);
    EXPECT_EQ(variant.toDouble(), 0.0);
    EXPECT_EQ(variant.toBool(), false);
    EXPECT_EQ(variant.toVariantList(), value);

    auto copyCtor(variant);
    EXPECT_EQ(copyCtor, variant);
    EXPECT_TRUE(copyCtor.isVariantList());
    EXPECT_EQ(copyCtor.toVariantList(), value);

    auto copyAffect = variant;
    EXPECT_EQ(copyAffect, variant);
    EXPECT_TRUE(copyAffect.isVariantList());
    EXPECT_EQ(copyAffect.toVariantList(), value);
  }
}

TEST_F(VariantTest, TestVariantNull)
{
  int nullDefaultValue = 0;

  Variant variant;
  EXPECT_TRUE(variant.isNull());
  EXPECT_EQ(variant.type(), Variant::EType::Null);
  EXPECT_EQ(variant.toInt(), nullDefaultValue);
  EXPECT_EQ(variant.toUnsignedInt(), nullDefaultValue);
  EXPECT_EQ(variant.toDouble(), (double)nullDefaultValue);
  EXPECT_EQ(variant.toBool(), (bool)nullDefaultValue);
  EXPECT_EQ(variant.toString(), std::string());
  EXPECT_EQ(variant.toVariantList(), std::list<Variant>());

  auto copyCtor(variant);
  EXPECT_EQ(copyCtor, variant);
  EXPECT_TRUE(copyCtor.isNull());

  auto copyAffect = variant;
  EXPECT_TRUE(copyCtor.isNull());
  EXPECT_EQ(copyAffect, variant);

  variant = variant;
  EXPECT_TRUE(variant.isNull());
}

TEST_F(VariantTest, TestVariantInteger)
{
  testVariantInt(0);
  testVariantInt(1);
  testVariantInt(-1);
  testVariantInt(std::numeric_limits<int>::min());
  testVariantInt(std::numeric_limits<int>::lowest());
  testVariantInt(std::numeric_limits<int>::max());
}

TEST_F(VariantTest, TestVariantUnsignedInteger)
{
  testVariantUnsignedInt(0);
  testVariantUnsignedInt(1);
  testVariantUnsignedInt(std::numeric_limits<unsigned int>::min());
  testVariantUnsignedInt(std::numeric_limits<unsigned int>::lowest());
  testVariantUnsignedInt(std::numeric_limits<unsigned int>::max());
}

TEST_F(VariantTest, TestVariantDouble)
{
  testVariantReal<double>(0.0);
  testVariantReal<double>(1.0);
  testVariantReal<double>(-1.0);
  testVariantReal<double>(std::numeric_limits<double>::min());
  testVariantReal<double>(std::numeric_limits<double>::lowest());
  testVariantReal<double>(std::numeric_limits<double>::max());
}

TEST_F(VariantTest, TestVariantFloat)
{
  testVariantReal<float>(0.0);
  testVariantReal<float>(1.0);
  testVariantReal<float>(-1.0);
  testVariantReal<float>(std::numeric_limits<float>::min());
  testVariantReal<float>(std::numeric_limits<float>::lowest());
  testVariantReal<float>(std::numeric_limits<float>::max());
}

TEST_F(VariantTest, TestVariantBool)
{
  testVariantBool(true);
  testVariantBool(false);
  testVariantBool(std::numeric_limits<bool>::min());
  testVariantBool(std::numeric_limits<bool>::lowest());
  testVariantBool(std::numeric_limits<bool>::max());
}

TEST_F(VariantTest, TestVariantString)
{
  testVariantString<std::string>("");
  testVariantString<std::string>("BimBamBoom");
  testVariantString<const char*>("");
  testVariantString<const char*>("BoomBamBim");
  EXPECT_EQ(Variant(std::string("BimBamBoom")).toString(), "BimBamBoom");
}

TEST_F(VariantTest, TestVariantList)
{
  std::list<Variant> listOfVariants;
  listOfVariants.push_back(0);
  listOfVariants.push_back(1);
  listOfVariants.push_back(-1);
  listOfVariants.push_back(std::numeric_limits<double>::min());
  listOfVariants.push_back(std::numeric_limits<double>::lowest());
  listOfVariants.push_back(std::numeric_limits<double>::max());
  listOfVariants.push_back("BimBoomBam");
  testVariantList(listOfVariants);

  listOfVariants.clear();
  listOfVariants.push_back(-1234);
  listOfVariants.push_back(1234u);
  listOfVariants.push_back(12.340000);
  listOfVariants.push_back(true);
  listOfVariants.push_back(false);
  listOfVariants.push_back("BimBamBoom");
  std::list<Variant> subListOfVariants;
  subListOfVariants.push_back("Bim");
  subListOfVariants.push_back("Bam");
  subListOfVariants.push_back("Boom");
  listOfVariants.push_back(subListOfVariants);
  EXPECT_EQ(Variant(listOfVariants).toString(), "[-1234, 1234, 12.340000, true, false, BimBamBoom, [Bim, Bam, Boom]]");
}

TEST_F(VariantTest, TestOperatorEqual)
{
  const Variant nullVariant;
  const Variant integerVariant(1234);
  const Variant doubleVariant(12.34);
  const Variant floatVariant((float)12.34);
  const Variant stringVariant("strVariant");
  const Variant boolVariant(true);
  std::list<Variant> listValues;
  listValues.push_back(1);
  listValues.push_back(2.3);
  listValues.push_back("4");
  const Variant variantList(listValues);

  EXPECT_NE(nullVariant, integerVariant);
}
