/* Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "SettingsTest.h"

#include "Settings.h"
#include "Log.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace Utils;
using namespace UtilsTest;

namespace Utils
{
  using SettingsType = SettingsWrapper<SettingsImpl,
                                       SettingsJsonParser,
                                       SettingsJsonSerializer>;
}

namespace
{
  const std::string KTestSettingFilePath {"./testSettings.json"};
  const std::string kJsonContent = R"({
  "list_values": [
    "val1",
    "val2",
    "val3"
  ],
  "setting0": {
    "double": 6.3,
    "integer": -1234,
    "string0": "string0",
    "string1": "string1"
  },
  "setting1": {
    "setting10": {
      "integer": 1
    },
    "string0": "string0",
    "string1": "string1"
  },
  "setting2": true
})";
}

TEST_F(SettingsTest, TestJsonSerializer)
{
  SettingsType settings;

  settings.set("list_values", std::list<Variant>({"val1", "val2", "val3"}));
  settings.set("setting0.double", 6.3);
  settings.set("setting0.integer", -1234);
  settings.set("setting0.string0", "string0");
  settings.set("setting1.string0", "string0");
  settings.set("setting0.string1", "string1");
  settings.set("setting1.setting10.integer", 1);
  settings.set("setting1.string1", "string1");
  settings.set("setting2.boolean", false);
  settings.set("setting2", true);

  settings.save(KTestSettingFilePath);

  std::ifstream testSettingsFile(KTestSettingFilePath, std::ios::in);
  EXPECT_TRUE(testSettingsFile.is_open());

  std::stringstream testSettingsData;
  testSettingsData << testSettingsFile.rdbuf();

  EXPECT_TRUE(testSettingsData.str() == kJsonContent);
}

TEST_F(SettingsTest, TestJsonParser)
{
  std::ofstream outputFile(KTestSettingFilePath, std::ios::out);
  outputFile << kJsonContent;
  outputFile.close();

  SettingsType settings;
  settings.load(KTestSettingFilePath);

  EXPECT_TRUE(settings.get("list_values").toVariantList() == std::list<Variant>({"val1", "val2", "val3"}));
  EXPECT_TRUE(settings.get("setting0.double").toDouble() == 6.3);
  EXPECT_TRUE(settings.get("setting0.integer").toDouble() == -1234);
  EXPECT_TRUE(settings.get("setting0.string0").toString() == "string0");
  EXPECT_TRUE(settings.get("setting1.string0").toString() == "string0");
  EXPECT_TRUE(settings.get("setting0.string1").toString() == "string1");
  EXPECT_TRUE(settings.get("setting1.setting10.integer").toInt() == 1);
  EXPECT_TRUE(settings.get("setting1.string1").toString() == "string1");
  EXPECT_TRUE(settings.get("setting2.null").isNull());
  EXPECT_TRUE(settings.get("setting2").toBool() == true);
}
