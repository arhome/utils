## Utils library {#mainpage}
A custom collection of C++ utilities.

## Author
**R. Doumenc** - [rdoumenc](https://gitlab.com/rdoumenc)

## License
This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/arhome/utils/blob/master/LICENSE) file for details.

[![pipeline status](https://gitlab.com/arhome/utils/badges/master/pipeline.svg)](https://gitlab.com/arhome/utils/commits/master) [![coverage report](https://gitlab.com/arhome/utils/badges/master/coverage.svg)](https://gitlab.com/arhome/utils/commits/master)
