# Copyright (c) 2017 - 2018 R.Doumenc remi.doumenc@gmail.com
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# Import Curlpp

find_package(CURL)
if(!CURL_FOUND)
  message(FATAL_ERROR "Could not find the CURL library and development files.")
endif()

configure_file(${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt.in
  curlpp-download/CMakeLists.txt
  )

# Download and unpack curlpp at configure time
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/curlpp-download
  )
if(result)
  message(FATAL_ERROR "CMake step for curlpp failed: ${result}")
endif()

execute_process(COMMAND ${CMAKE_COMMAND} --build .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/curlpp-download
  )
if(result)
  message(FATAL_ERROR "Build step for curlpp failed: ${result}")
endif()

# Add curlpp directly to our build.
add_subdirectory(${CMAKE_BINARY_DIR}/curlpp-src
  ${CMAKE_BINARY_DIR}/curlpp-build
  )

include_directories(${CMAKE_BINARY_DIR}/curlpp-src/include)
